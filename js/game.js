
var img = null;
var context = null;
var framerate = 100;
var x = 0;
var sx = 0;

var imageLoaded = function() {
	console.log("Image Loaded "+img.height);
	//context.drawImage(img, sx, 0); //, 300, 300, 0, 0, 300, 300);
	setInterval(animate, framerate);
}

window.onload = function() {
	var body = document.getElementById("body");
	var canvas = document.createElement("canvas");
	
	canvas.width = 120; canvas.height = 300;
	context = canvas.getContext("2d");
	body.appendChild(canvas);
	
	img = new Image();
	img.onload = imageLoaded;
	img.src = "img/person.png";
	
}

var animate = function() {

	context.clearRect(0, 0, 120, 300);
	context.drawImage(img, sx, 0, 120, img.height, 0, 0, 120, img.height); //, 300, 300, 0, 0, 300, 300);
	sx = (sx+120)%500;
}
